
/**
 * Start my codez
 */
require(['dojo/Deferred', 'dojo/request/xhr', 'dojo/dom-construct',
    'dojo/aspect', 'dojo/on', 'dijit/registry', 'dojo/parser',
    'dojo/_base/event', 'dojo/dom-form', 'dojo/dom-class', 'dojo/query', 'dojo/ready',
    "https://connect.facebook.net/en_US/all.js"],
    function(Deferred, xhr, domConstruct, aspect, on, registry, parser, ev,
      domForm, domClass, query, ready, facebook) {
        ready(function() {
          parser.parse();
          window.lunch = {} //my global namespace
          /**
           * Facebook Stuffs
           */
          // Additional Facebook JS functions here
          window.fbAsyncInit = function() {
            console.log(FB);


            // Additional init code here
            FB.getLoginStatus(function(response) {
              if (response.status === 'connected') {
                // connected
                console.log('connected');

              } else if (response.status === 'not_authorized') {
                console.log('not authorized');
                login();
              } else {
                console.log('IDK');
                login();
              }
            });

          };

          var deferred = new Deferred(/*TODO Do Something if cancelled*/);

          /*
             Facebook Login function
             */
          function login() {
            FB.login(function(response) {
              console.log('FB login');
              console.log(response);
              if (response.authResponse) {
                // connected
                //Fullfills deferred to start-up page setup
                registry.byId('loginDialog').hide();
                domClass.add("loginErr", 'hidden');
                deferred.resolve();
              } else {
                domClass.remove("loginErr", 'hidden');
                // cancelled
              }
            });
          }
          /*
           * Facebook Initialization
           */
          FB.init({
            appId: '375633432535223', // App ID
            channelUrl: 'https://localhost:5000/channel.html', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
          });

          //Login as soon as I can need not popup
          registry.byId('loginDialog').show();
          //Launch Facebook Login on button press
          on(document.getElementById("loginBut"), 'click', function(e){
            login();
          });

          /**
           * grabNotifs from server
           */
          function grabNotifs(){
            //TODO
            deferred.then(function(loginResponse) {
              var promise = xhr('/request/', {
                handleAs: 'json'
              }).then(function(data) {
                for (notif in data) {
                  domContstruct.place(createListItem(notif), 'profferList',
                    'last');
                }
              }, function(err) {
                showErrorMsg();
              });
            }, function(err) {
              /*TODO Handle that error*/
            });
          }

          var notifInterval;
          notifInterval = setInterval(grabNotifs, 10*1000);

          //Connect click to proffer Dialog
          on(document.getElementById('profBut'), 'click', function(e) {
            var profDialog = registry.byId('profferDialog');
            profDialog.show();
          });
          //Connect proffer Dialog form to notification
          on(document.getElementById('profferForm'), 'submit', function(e) {
            ev.stop(e);
            formObj = domForm.toJson('profferForm');
            formObj['userId'] =//TODO I need an id
            xhr.post(
              {
                data: formObj
              });
          });

          //Switch time on/off
          on(
              query("form#profferForm input[name='time']")[0], 'change',
              function(e) {
                if (e.target.checked == true) {
                  query('.timeForm').forEach(function(node) {
                    domClass.add(node, 'enabled');
                    domClass.remove(node, 'disabled');
                    registry.byNode(node).set('disabled', true);
                  });
                }else {
                  query('.timeForm').forEach(function(node) {
                    domClass.remove(node, 'enabled');
                    domClass.add(node, 'disabled');
                    registry.byNode(node).set('disabled', false);
                  });
                }
                console.log(domForm.toJson(document.getElementById('profferForm')));
              });

          function createListItem(notif) {
            return domConstruct.toDom('<li>'+ notif['name'] + ': ' + notif['text'] + '</li>');
          }
          function showErrorMsg() {
            alert('Dagnabit, something went wrong');
          }
        });
      });
