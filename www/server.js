var gcm = require('node-gcm');
var https = require('https');
var mongoose = require('mongoose');
var express = require('express');
var redis = require('redis').createClient();
var RedisStore = require('connect-redis')(express);
var passport = require('passport');

var fs = require('fs');
var crypto = require('crypto');
var server;

var config = require('./config.json');
var api = require('./api');
var models = require('./models');

var message = new gcm.Message();
var sender = new gcm.Sender(config.google.server.apiKey);
var app = express();

mongoose.connect(config.database.uri);

app.configure(function() {
  app.set('port', process.env.PORT || 443);
  app.use(express.cookieParser(config.session.secret));
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.session({
    secret: config.session.secret,
    store: new RedisStore({
      client: redis
    })
  }));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(app.router);
  app.use(express.csrf());
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function() {
  app.use(express.errorHandler({
    dumbExceptions: true,
    showStack: true
  }));
});

app.configure('production', function() {
  app.use(express.errorHandler());
});

models.configure(mongoose, config);
api.configure(app);



var privateKey = fs.readFileSync('privatekey.pem').toString();
var certificate = fs.readFileSync('certificate.pem').toString();
var credentials = {key: privateKey, cert: certificate};
// Start the server
server = https.createServer(credentials, app).listen(app.get('port'));
