var config = require('./config.json');
exports.configure = function(db) {
  var UserSchema = new db.Schema({
    displayName: String,
    facebookId: String,
    facebookFriends: [String]
  });
  exports.User = db.model('User', UserSchema);

  var BeaconSchema = new db.Schema({
    beaconType: String,
    duration: Number,
    startTime: Number,
    active: {
      type: Boolean,
      default: true
    },
    participants: [String],
    author: String
  });
  exports.Beacon = db.model('Beacon', BeaconSchema);
}
