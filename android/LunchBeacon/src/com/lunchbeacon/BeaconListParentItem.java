package com.lunchbeacon;

import java.util.ArrayList;

import android.graphics.drawable.Drawable;

public class BeaconListParentItem {
	private String mTitle;
	private ArrayList<String> mArrayChildren;
	private Drawable pic;

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String mTitle) {
		this.mTitle = mTitle;
	}

	public ArrayList<String> getArrayChildren() {
		return mArrayChildren;
	}

	public void setArrayChildren(ArrayList<String> mArrayChildren) {
		this.mArrayChildren = mArrayChildren;
	}

	public Drawable getPicture() {
		return pic;
	}

	public void setPicture(Drawable d) {
		this.pic = d;
	}
}