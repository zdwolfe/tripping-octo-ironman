var models = require('./models');
var helpers = require('./helpers');
var passport = require('passport');
var config = require('./config.json');
var FacebookStrategy = require('passport-facebook').Strategy;
var FB = require('fb');

exports.configure = function(app) {

  passport.serializeUser(function(user, done) {
      done(null, user);
  });

  passport.deserializeUser(function(obj, done) {
      done(null, obj);
  });

  passport.use(new FacebookStrategy({
      clientID: config.facebook.id,
      clientSecret: config.facebook.secret,
      callbackURL: 'https://zachwolfe.org/auth/facebook/callback'
    },
    function(accessToken, refreshToken, profile, done) {

      var newDisplayName = '';
      if (profile && profile.displayName) {
        newDisplayName = profile.displayName;
      }
      models.User.findOne({facebookId: profile.id}).exec(function(err, user) {
        if (err) { return done(err); }
  
        FB.setAccessToken(accessToken);
        FB.api('/me/friends', {fields: 'id'}, function(res) {
          var ids=[];
          for (var i = 0; i < res.data.length; i++) {
            ids.push(res.data[i].id);
          }
          console.log('ids = ' + JSON.stringify(ids));
          if (user) {
            var returnUser = {
              displayName: newDisplayName,
              fbRefreshToken: refreshToken,
              id: user._id,
              facebookFriends: ids
            };
            return done(null, returnUser);
          }
          var newUser = new models.User({
            displayName: newDisplayName,
            facebookId: profile.id,
            facebookFriends: ids
          });
          newUser.save(function(err) {
            if (err) { return done(err); }
            return done(null, newUser);
          });
        });
      });
    }
  ));

  app.get('/logout', function(req, res) {
      req.logout();
        res.redirect('/');
  });

  app.get('/auth/facebook',
    passport.authenticate('facebook', {display: 'popup', scope: ['user_events', 'email']}));

  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/',
                                        failureRedirect: '/facebookFail'}));

  app.get('/auth/facebook/success', function(req, res) {
    console.log('req.user = ' + JSON.stringify(req.user));
    res.send(req.user, 200);
  });

  app.get('/auth/facebook/fail', function(req, res) {
    res.send(401);
  });

  app.get('/', function(req, res) {
    console.log('got /');
    res.redirect('/index.html');
  });


  app.get('/beacons', ensureAuthenticated, function(req, res) {
    var user = req.user;
    if (!user) { return res.send('not logged in', 401); }
    models.User.find({ facebookId: {$in: user.facebookFriends}}).exec(function(err, users) {
      if (err) { return helpers.returnError(res, 'error finding users by facebook id array'); }
      var ids=[];
      for (var i = 0; i < users.length; i++) {
        if (users[i].id != user.id) {
          ids.push(users[i].id);
        }
      }
      console.log('ids = ' + JSON.stringify(ids));
      models.Beacon.find({author: {$in: ids}, active: true}).exec(function(err, beacons) {
        if (err) { return helpers.returnError(res, 'error finding beacons by user array'); }
        return res.json(beacons);
      });
    });
  });

  app.get('/me', ensureAuthenticated, function(req, res) {
    return res.json(req.user);
  });

  app.get('/user/:id', function(req, res) {
    var id = req.params.id;
    if (!id) {
      return helpers.returnError(req, 'user must have id parameter');
    }
    models.User.findById(id).exec(function(err, user) {
      if (!user) { return helpers.returnError(res, 'error finding user by id '); }
      if (err) { return helpers.returnError(res, 'error finding user by id ' + error); }
      return res.json(user);
    });
  });

  app.get('/beacon/:id', function(req, res) {
    var id = req.params.id;
    if (!id) {
      return helpers.returnError(req, 'beacon must have id parameter');
    }
    models.Beacon.findById(id).exec(function(err, beacon) {
      if (err) { return helpers.returnError(res, 'error finding beacon by id ' + err); }
      if (beacon) {
        return res.json(beacon);
      }
      return res.send(null, 404);
    });
  });

  app.post('/beacon', ensureAuthenticated, function(req, res) {
    var authorId = req.user.id;
    var typeId = req.body.type;
    var newDuration = req.body.duration || 60;
    var newStartTime = req.body.startTime || Math.round((new Date()).getTime() / 1000);

    if (!typeId) { return helpers.returnError(res, 'must have type'); }

    if (!authorId) { return res.send(null, 401); }

    models.Beacon.findOne({author: authorId}).exec(function(err, beacon) {
      if (beacon) {
        // User already has an active beacon so return with it
        beacon.active = true;
        beacon.save(function(err) {
          if (err) { return helpers.returnError(res, 'could not update existing beacon'); }
          return res.json({
            id: beacon._id
          });
        });
      } else {
        var newBeacon = new models.Beacon({
          author: authorId,
          type: typeId,
          duration: newDuration,
          startTime: newStartTime,
          participants: [req.user.id]
        });
        console.log('about to save newBeacon');
        newBeacon.save(function(err) {
          if (err) { return helpers.returnError(res, 'error making new beacon' + err); }
          return res.json({
            id: newBeacon._id
          });
        });
      }
    });
  });

  app.del('/beacon/:id', ensureAuthenticated, function(req, res) {
    var user = req.user;
    var id = req.params.id;
    console.log('id = ' + id);
    if (!user) { return res.send(401); }
    if (!id) { return helpers.returnError(res, 'must include id'); }
    models.Beacon.findById(id).exec(function(err, beacon) {
      if (err) { return helpers.returnError(res, 'error finding beacon by id to quit ' + err); }
      if (!beacon) { return res.send(null, 404); }
      if (beacon.participants.length <= 0) {
        return res.send(304); 
      } else {
        for (var i = 0; i < beacon.participants.length; i++) {
          if (beacon.participants[i] == user.id) {
            if (beacon.participants.length == 1) {
              console.log('should remove!');
              models.Beacon.findOne({_id: beacon.id}).remove();
              return res.send(null, 202);
            } else {
              beacon.participants.splice(i,1);
              beacon.save(function(err) {
                console.log('about to 202.  beacon.participants = ' + beacon.participants);
                if (err) { return helpers.returnError(res, 'error saving beacon'); }
                return res.send(null, 202);
              });
            }
          }
        }
      }
    });
  });

  app.post('/join/beacon/:id', ensureAuthenticated,  function(req, res) {
    var id = req.params.id;
    var user = req.user;
    if (!id) { return helpers.returnError(res, 'joining a beacon requires a beacon id'); }
    models.Beacon.findById(id).exec(function(err, beacon) {
      if (err) { return helpers.returnError(res, 'error when finding beacon by id'); }
      if (!beacon) { return res.send(404); }

      for (var i = 0; i < beacon.participants.length; i++) {
        if (beacon.participants[i] == user.id) {
          return res.send(null, 304);
        }
      }

      beacon.participants.push(user.id);
      beacon.save(function(err) {
        if (err) { return helpers.returnError(res, 'error saving beacon'); }
        return res.send(null, 200);
      });
    });
  });
};

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.send('must be logged in', 401);
}
