package com.lunchbeacon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TimePicker;

public class CreateBeaconActivity extends Activity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // No top bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_create_beacon);
        

        // Make Time Picker hide am/pm dialog
        TimePicker tp = (TimePicker) findViewById(R.id.createBeaconDurationPicker);
        tp.setIs24HourView(true);
        tp.setCurrentHour(1);
        tp.setCurrentMinute(0);
        
        // Create Button Listener
        ImageButton bt = (ImageButton)findViewById(R.id.createBeaconButton);
        
        android.view.View.OnClickListener createBeaconClickListener = new View.OnClickListener() {
            public void onClick(View v) {
               Intent i = new Intent(CreateBeaconActivity.this,
                     BeaconStatusActivity.class);
               CreateBeaconActivity.this.startActivity(i);
            }
         };
        
        bt.setOnClickListener(createBeaconClickListener);
        
	}
}
