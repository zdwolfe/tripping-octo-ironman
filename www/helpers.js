exports.returnError = function(res, msg) {
  if (!msg) {
    msg = 'an error occured';
  }
  if (res) {
    return res.json({
      error: msg
    });
  }
  return res.json(500);
};
