package com.lunchbeacon;


import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class BeaconStatusActivity extends Activity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // No top bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_beacon_status);
        
        ListView lv = (ListView) findViewById(R.id.beaconStatusParticListView);
        
        ArrayAdapter<String> ar = new ArrayAdapter<String>(this, R.layout.participant_child_item);
        ar.add("Zach");
        ar.add("Paul");
        ar.add("Max");
        lv.setAdapter(ar);

	}
}
