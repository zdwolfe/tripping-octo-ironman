package com.lunchbeacon;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;

public class MainListActivity extends Activity {

   public static final String LOG_TAG = "LBD";
   
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      // Remove notification and title bar
      this.requestWindowFeature(Window.FEATURE_NO_TITLE);
      this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);

      setContentView(R.layout.activity_mainlist);

      ExpandableListView beaconsLV = (ExpandableListView) findViewById(R.id.mainListLV);
      ArrayList<BeaconListParentItem> parents = new ArrayList<BeaconListParentItem>();

      // Obama
      BeaconListParentItem ob = new BeaconListParentItem();
      ob.setTitle("Obama is Hungry");
      ArrayList<String> ochild = new ArrayList<String>();
      ochild.add("Hamburger Hutt\n3:00am");
      ob.setArrayChildren(ochild);
      ob.setPicture(getResources().getDrawable(R.drawable.obama));
      parents.add(ob);

      for (int i = 0; i < 30; i++) {
         BeaconListParentItem p = new BeaconListParentItem();
         p.setTitle("Person " + Integer.toString(i) + " wants to get lunch!");

         // Children
         ArrayList<String> children = new ArrayList<String>();
         children.add(new String("Comet Coffee\n3:00pm"));

         p.setArrayChildren(children);
         p.setPicture(getResources().getDrawable(R.drawable.obama));
         parents.add(p);

      }
      BeaconListAdapter adapter = new BeaconListAdapter(MainListActivity.this,
            parents);
      beaconsLV.setAdapter(adapter);

      // Make the Plus TV into a button
      Button createBeaconTV = (Button) findViewById(R.id.mainListNewButtonTV);

      android.view.View.OnClickListener createBeaconClickListener = new View.OnClickListener() {
         public void onClick(View v) {
            Intent i = new Intent(MainListActivity.this,
                  CreateBeaconActivity.class);
            MainListActivity.this.startActivity(i);
         }
      };
      createBeaconTV.setOnClickListener(createBeaconClickListener);

      Session.openActiveSession(this, false, new StatusCallback() {

         @Override
         public void call(Session session, SessionState state,
               Exception exception) {
            // TODO Auto-generated method stub

            Log.d(LOG_TAG, session + " " + state + " " + exception);
         }

      });

   }

   @Override
   public void onBackPressed() {
      Log.d(LOG_TAG, "back");
   }

}
