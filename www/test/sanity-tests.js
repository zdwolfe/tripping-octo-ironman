var https = require('https');
var mongoose = require('mongoose');

var fs = require('fs');
var crypto = require('crypto');
var server;

var config = require('./../config.json');
var api = require('./../api');
var models = require('./../models');

console.log('GET /');

https.get('https://localhost:5000/', function(res) {
  console.log('statusCode: ', res.statusCode);
  console.log('headers: ', res.headers);

  res.on('data', function(d) {
    process.stdout.write(d);
    process.stdout.write('\n');
  });

}).on('error', function(e) {
  console.error(e);
});
