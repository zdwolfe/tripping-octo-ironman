package com.lunchbeacon;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.Settings;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class LoginActivity extends Activity {

   public static final String LOG_TAG = "LBD";
   public static String FB_AT_KEY_NAME = "FB_AT";
   final private StatusCallback SessionStatusCallback = new SessionStatusCallback();

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      /* initialize UI */
      this.requestWindowFeature(Window.FEATURE_NO_TITLE);
      this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);

      setContentView(R.layout.activity_login);
      Typeface tf = Typeface.createFromAsset(this.getAssets(),
            "fonts/AveriaLibre-Bold.ttf");
      TextView appTitleTV = (TextView) findViewById(R.id.splashtv);
      appTitleTV.setTypeface(tf);

      /* debug aids */
      Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
      
      /* this value is used in the facebook developer tools' signatures
       * field.
       */
      try {
         PackageInfo info = getPackageManager().getPackageInfo(
               "com.lunchbeacon", PackageManager.GET_SIGNATURES);
         for (Signature signature : info.signatures) {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(signature.toByteArray());

            Log.d(LOG_TAG,
                  Base64.encodeToString(md.digest(), Base64.DEFAULT));
         }
      } catch (NameNotFoundException e) {
         Log.d(LOG_TAG, e.getMessage());

      } catch (NoSuchAlgorithmException e) {
         Log.d(LOG_TAG, e.getMessage());
      }

      /* initiate oauth flows */
      Session.openActiveSession(this, true, SessionStatusCallback);
   }

   protected void leaveSplash(String fbAt) {
      Intent homeIntent = new Intent(this, MainListActivity.class);
      homeIntent.putExtra(FB_AT_KEY_NAME, fbAt);
      startActivity(homeIntent);
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      Session.getActiveSession().onActivityResult(this, requestCode,
            resultCode, data);
   }

   /* this nested class wraps state transitions */
   private class SessionStatusCallback implements Session.StatusCallback {
      @Override
      public void call(Session session, SessionState state, Exception exception) {

         Log.d(LOG_TAG, session + " " + state + " " + exception);

         if (state == SessionState.OPENED) {
            leaveSplash(session.getAccessToken());
         }
      }
   }
}